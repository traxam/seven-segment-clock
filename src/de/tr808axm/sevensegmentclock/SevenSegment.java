package de.tr808axm.sevensegmentclock;

/**
 * Represents a seven-segmented display.
 * Created by tr808axm on 08.07.2016.
 * (https://en.wikipedia.org/wiki/File:7_segment_display_labeled.svg)
 */
public class SevenSegment {
    public static final SevenSegment ZERO = new SevenSegment(new boolean[]{true, true, true, true, true, true, false});
    public static final SevenSegment ONE = new SevenSegment(new boolean[]{false, true, true, false, false, false, false});
    public static final SevenSegment TWO = new SevenSegment(new boolean[]{true, true, false, true, true, false, true});
    public static final SevenSegment THREE = new SevenSegment(new boolean[]{true, true, true, true, false, false, true});
    public static final SevenSegment FOUR = new SevenSegment(new boolean[]{false, true, true, false, false, true, true});
    public static final SevenSegment FIVE = new SevenSegment(new boolean[]{true, false, true, true, false, true, true});
    public static final SevenSegment SIX = new SevenSegment(new boolean[]{true, false, true, true, true, true, true});
    public static final SevenSegment SEVEN = new SevenSegment(new boolean[]{true, true, true, false, false, false, false});
    public static final SevenSegment EIGHT = new SevenSegment(new boolean[]{true, true, true, true, true, true, true});
    public static final SevenSegment NINE = new SevenSegment(new boolean[]{true, true, true, true, false, true, true});

    private enum Segment {
        A((short) 0),
        B((short) 1),
        C((short) 2),
        D((short) 3),
        E((short) 4),
        F((short) 5),
        G((short) 6);

        private final short number;

        Segment(short number) {
            this.number = number;
        }

        public short getNumber() {
            return number;
        }
    }

    private final boolean[] segments;

    public SevenSegment(boolean[] segments) {
        this.segments = segments;
    }

    public String[] toStrings() {
        String[] strings = new String[3];
        strings[0] = segments[Segment.A.getNumber()] ? " _ " : "   ";
        strings[1] = (segments[Segment.F.getNumber()] ? "|" : " ") + (segments[Segment.G.getNumber()] ? "_" : " ") + (segments[Segment.B.getNumber()] ? "|" : " ");
        strings[2] = (segments[Segment.E.getNumber()] ? "|" : " ") + (segments[Segment.D.getNumber()] ? "_" : " ") + (segments[Segment.C.getNumber()] ? "|" : " ");
        return strings;
    }

    public static String[] concatSevenSegmentStrings(String[][] sevenSegmentStringsArray) {
        String[] strings = new String[sevenSegmentStringsArray[0].length];
        for(int i = 0; i < strings.length; i++) {
            strings[i] = "";
            for(String[] sevenSegmentStrings : sevenSegmentStringsArray) {
                strings[i] += sevenSegmentStrings[i];
            }
        }
        return strings;
    }

    public static SevenSegment parseNumber(char character) {
        switch (character) {
            case '0':
                return ZERO;
            case '1':
                return ONE;
            case '2':
                return TWO;
            case '3':
                return THREE;
            case '4':
                return FOUR;
            case '5':
                return FIVE;
            case '6':
                return SIX;
            case '7':
                return SEVEN;
            case '8':
                return EIGHT;
            case '9':
                return NINE;
            default:
                throw new IllegalArgumentException("character has to be a number");
        }
    }
}
