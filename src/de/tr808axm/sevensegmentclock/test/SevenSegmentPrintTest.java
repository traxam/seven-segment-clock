package de.tr808axm.sevensegmentclock.test;

import de.tr808axm.sevensegmentclock.SevenSegment;
import de.tr808axm.sevensegmentclock.SevenSegmentClock;

import static de.tr808axm.sevensegmentclock.SevenSegmentClock.printSevenSegmentStrings;

/**
 * Prints all predefined SevenSegments.
 * Created by tr808axm on 08.07.2016.
 */
class SevenSegmentPrintTest {
    public static void main(String[] args) {
        System.out.println("Predefined numbers: ");
        printSevenSegmentStrings(SevenSegment.ZERO.toStrings());
        printSevenSegmentStrings(SevenSegment.ONE.toStrings());
        printSevenSegmentStrings(SevenSegment.TWO.toStrings());
        printSevenSegmentStrings(SevenSegment.THREE.toStrings());
        printSevenSegmentStrings(SevenSegment.FOUR.toStrings());
        printSevenSegmentStrings(SevenSegment.FIVE.toStrings());
        printSevenSegmentStrings(SevenSegment.SIX.toStrings());
        printSevenSegmentStrings(SevenSegment.SEVEN.toStrings());
        printSevenSegmentStrings(SevenSegment.EIGHT.toStrings());
        printSevenSegmentStrings(SevenSegment.NINE.toStrings());

        System.out.println("Concatenated numbers: 82, 041, 8421");
        printSevenSegmentStrings(SevenSegment.concatSevenSegmentStrings(new String[][]{SevenSegment.EIGHT.toStrings(), SevenSegment.TWO.toStrings()}));
        printSevenSegmentStrings(SevenSegment.concatSevenSegmentStrings(new String[][]{SevenSegment.ZERO.toStrings(), SevenSegment.FOUR.toStrings(), SevenSegment.ONE.toStrings()}));
        printSevenSegmentStrings(SevenSegment.concatSevenSegmentStrings(new String[][]{SevenSegment.EIGHT.toStrings(), SevenSegment.FOUR.toStrings(), SevenSegment.TWO.toStrings(), SevenSegment.ONE.toStrings()}));

        System.out.println("Insert non-seven segment string (colon as three-line-string): 15:30");
        printSevenSegmentStrings(SevenSegment.concatSevenSegmentStrings(new String[][]{SevenSegment.ONE.toStrings(), SevenSegment.FIVE.toStrings(), SevenSegmentClock.colonStrings, SevenSegment.THREE.toStrings(), SevenSegment.ZERO.toStrings()}));

        System.out.println("Print time: ");
        SevenSegmentClock.printSevenSegmentStrings(SevenSegmentClock.convertTimeToSevenSegmentStrings());
    }
}
